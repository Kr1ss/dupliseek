#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='DupliSeek',
	version='0.0.2',
	license="MIT",
	description='Duplicate image finder',
	author='Magnus Jørgensen',
	author_email='magnusmj@gmail.com',
	url='https://gitlab.com/magnusmj/dupliseek',
	py_modules=["DupliSeek/main"],
	entry_points={
		'console_scripts': [
			'dupliseek = main:main',
		],
		'gui_scripts': [
			'dupliseek = main:main',
		]
	},
	package_dir={
		'dupliseek': 'DupliSeek',
	},
	packages=[
		'DupliSeek.GUI',
		'DupliSeek.GUI.Ribbon',
		'DupliSeek.GUI.Widgets',
		'DupliSeek.icons',
		'DupliSeek.stylesheets'
	],
	package_data={
		'': ['*.png', '*.css'],
	}
)
