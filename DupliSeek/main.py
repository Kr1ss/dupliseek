#!/usr/bin/python3
import sys
from PyQt5.QtWidgets import *
from GUI.MainWindow import MainWindow
import os

__author__ = 'magnus jørgensen'
__version__ = '0.0.2'


a = QApplication(sys.argv)


def except_hook(cls, exception, traceback):
	sys.__excepthook__(cls, exception, traceback)


def main():
	a.setQuitOnLastWindowClosed(True)
	main_window = MainWindow(a)
	main_window.show()
	sys.exit(a.exec())


sys.excepthook = except_hook

if __name__ == '__main__':
	main()

