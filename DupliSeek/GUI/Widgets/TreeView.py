from PyQt5.QtCore import QItemSelectionRange, Qt
from PyQt5.QtWidgets import QDockWidget, QTreeView, QFileSystemModel

from GUI import tr, gui_scale


class TreeViewDock(QDockWidget):
	def __init__(self, main_window):
		QDockWidget.__init__(self, main_window)
		self._main_window = main_window
		self.setWindowTitle(tr("Folder view"))
		self.setObjectName("TreeViewDock")
		self.setMinimumWidth(250*gui_scale())

		self._filters = []
		for ending in self._main_window._image_endings:
			self._filters.append("*" + ending)

		self._treeView = QTreeView(self)
		self.setWidget(self._treeView)


	def set_folder(self, path):
		self._model = QFileSystemModel()
		self._model.setNameFilters(self._filters)
		self._model.setNameFilterDisables(False)
		self._model.setRootPath(path)
		self._treeView.setModel(self._model)
		self._treeView.setRootIndex(self._model.index(path))
		self._treeView.setColumnWidth(0, 250*gui_scale())
		self._treeView.selectionModel().selectionChanged.connect(self.on_tree_selection_changed)


	def on_tree_selection_changed(self, selection):

		for selrange in selection:
			for sel in selrange.indexes():
				path = self._model.filePath(sel)
				self._main_window.on_tree_selection_changed(path)
				return